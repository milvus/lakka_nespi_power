# lakka_nespi_power

## about

Lakka 2.3 + NESPi Case PLUS + Shutdown/Reset Buttons

Adding the ability of soft shutdown to lakka and the NESPI CASE+ on the Raspberry Pi B+

Forked from https://github.com/marcelonovaes/lakka_nespi_power

Uses precompiled RPi.GPIO v0.6.5 https://pypi.org/project/RPi.GPIO/

## install

1. Make sure Rraspberry Pi is connected to the internet.

2. Make sure SSH is enabled in Lakka (Config / Services / SSH Enable is ON).

3. ssh into lakka:

   ```bash
   ssh root@YOUR_LAKKA_IP_ADDRESS
   password: root (if unchanged)
   ```

4. In the terminal, type the one-line command below (Case sensitive):

   wget -O - "https://gitlab.com/milvus/lakka_nespi_power/-/raw/master/install.sh" | bash

5. Your Raspberry Pi will reboot and you're done!

