#!/bin/bash

# Step 1) Check if root
if [[ $EUID -ne 0 ]]; then
   echo "Please execute script as root."
   exit 1
fi

#-----------------------------------------------------------

# Step 2) Go to the right folder
cd /storage/

#-----------------------------------------------------------

# Step 3) Download this repository with all files needed
wget "https://gitlab.com/milvus/lakka_nespi_power/-/archive/master/lakka_nespi_power-master.zip"

#-----------------------------------------------------------

# Step 4) Unpack files
unzip -o lakka_nespi_power-master.zip
rm lakka_nespi_power-master.zip
cd lakka_nespi_power-master/
cp -R lib/ /storage/
mkdir -p /storage/scripts
cp -R scripts/shutdown.py -t /storage/scripts/

if [ ! -f /storage/.config/autostart.sh ]; then
	cp .config/autostart.sh -t /storage/.config/
fi

if ! grep -Fxq "shutdown.py" /storage/.config/autostart.sh
then
	printf "\npython /storage/scripts/shutdown.py &" >> /storage/.config/autostart.sh
fi

echo "SUCCESS! Shutdown Script configured!"
cd /storage/
rm -r lakka_nespi_power-master/
echo "Installation done. Will now reboot after 3 seconds."
sleep 3
reboot
